//import add, subtract, multiply and divide functions from respective modules
var add = require('./add');
var subtract = require('./sub');
var multiply = require('./multiply');
var divide = require('./divide');

//Initialize two variables with numbers
var num1 = 10;
var num2 = 4;

//Call the functions in different modules and print the result
console.log("Addition of two numbers = ", add(num1,num2))
console.log("Subtraction of two numbers = ", subtract(num1,num2))
console.log("Multiplication of two numbers = ", multiply(num1,num2))
console.log("Division of two numbers = ", divide(num1,num2))