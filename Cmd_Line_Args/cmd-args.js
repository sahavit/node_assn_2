let status = true;
let result;
var strList = process.argv.slice(2);
var numList = [];
var resultOp;

let operation = strList[strList.length-1];

for(let i=0; i<strList.length-1; i++){
    let num = Number(strList[i]);
    if(isNaN(num)){
        status=false;
        break;
    }
    numList.push(num);
}
if(status==false){
    console.log("The operation cannot be performed.")
}
else{
    result=numList[0];
    for(let i=1; i< numList.length; i++){

        if(operation=='add'){
        result+=numList[i];
        resultOp="Addition"
        }

        else if(operation=='sub'){
            result-=numList[i];
            resultOp="Subtraction"
        }

        else if(operation=='mul'){
            result*=numList[i];
            resultOp="Multiplication"
        }
        else if(operation=='div'){
            result=result/numList[i];
            resultOp="Division"
        }
        else{
            resultOp="Invalid";            
        }
    };
    if(resultOp=="Invalid")
        console.log("Invalid operator. Enter only 'add' 'sub' 'mul' 'div'")
    else
        console.log(resultOp,"of numbers:",result)

}