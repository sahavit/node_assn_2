//import events
var events = require('events');

//Create event object
var eventEmitter = new events.EventEmitter();

//function which is called when event is emitted
function printEvent(){
    console.log("Event is emitted!")
}

//Registering the event
eventEmitter.on("newEvent", printEvent)

//Emitting the event every5s
setInterval( () => {eventEmitter.emit("newEvent")}, 5000);